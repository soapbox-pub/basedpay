import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :basedpay, Basedpay.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "basedpay_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :basedpay, BasedpayWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "9hD/MPMdfRXl0ivJN7gKsbpGXh3ysW7b+JzlOIlAyRE8e2db6yxny5tcaV8WsV7a",
  server: false

# In test we don't send emails.
config :basedpay, Basedpay.Mailer, adapter: Swoosh.Adapters.Test

# Disable Oban plugins and queues in tests.
config :basedpay, Oban, queues: false, plugins: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
