defmodule Basedpay.Repo do
  use Ecto.Repo,
    otp_app: :basedpay,
    adapter: Ecto.Adapters.Postgres
end
