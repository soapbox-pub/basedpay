defmodule Basedpay.Project do
  use Ecto.Schema
  alias Basedpay.Tier
  alias Basedpay.User
  import Ecto.Changeset

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "projects" do
    belongs_to(:user, User, type: FlakeId.Ecto.Type)
    field(:name, :string, default: "")
    field(:content, :string, default: "")
    has_many(:tiers, Tier)

    timestamps()
  end

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [])
    |> validate_required([])
  end
end
