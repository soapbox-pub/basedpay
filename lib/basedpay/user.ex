defmodule Basedpay.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "users" do
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [])
    |> validate_required([])
  end
end
