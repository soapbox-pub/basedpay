defmodule Basedpay.Tier do
  use Ecto.Schema
  alias Basedpay.Project
  import Ecto.Changeset

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "tiers" do
    belongs_to(:project, Project, type: FlakeId.Ecto.Type)
    field(:name, :string, default: "")
    field(:content, :string, default: "")
    field(:money, Money.Ecto.Map.Type)

    timestamps()
  end

  @doc false
  def changeset(tier, attrs) do
    tier
    |> cast(attrs, [])
    |> validate_required([])
  end
end
