# Gringotts.Money implementation for the Money library.
# - https://hexdocs.pm/gringotts/Gringotts.Money.html
# - https://github.com/elixirmoney/money

defimpl Gringotts.Money, for: Money do
  def currency(%Money{currency: currency}) do
    Atom.to_string(currency)
  end

  def value(%Money{} = money) do
    Money.to_decimal(money)
  end

  def to_integer(%Money{amount: amount} = money) do
    {currency(money), amount, exponent(money)}
  end

  def to_string(%Money{} = money) do
    {currency(money),
     Money.to_string(money, symbol: false, separator: "", strip_insignificant_zeros: true)}
  end

  defp exponent(%Money{currency: currency}) do
    -Money.Currency.exponent(currency)
  end
end
