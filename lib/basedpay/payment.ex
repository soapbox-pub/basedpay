defmodule Basedpay.Payment do
  use Ecto.Schema

  alias Basedpay.Project
  alias Basedpay.Subscription
  alias Basedpay.User

  import Ecto.Changeset

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "payments" do
    belongs_to(:user, User, type: FlakeId.Ecto.Type)
    belongs_to(:subscription, Subscription, type: FlakeId.Ecto.Type)
    belongs_to(:project, Project, type: FlakeId.Ecto.Type)
    field(:money, Money.Ecto.Map.Type)

    timestamps()
  end

  @doc false
  def changeset(payment, attrs) do
    payment
    |> cast(attrs, [])
    |> validate_required([])
  end
end
