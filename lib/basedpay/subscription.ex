defmodule Basedpay.Subscription do
  use Ecto.Schema

  alias Basedpay.Project
  alias Basedpay.Tier
  alias Basedpay.User

  import Ecto.Changeset

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "subscriptions" do
    belongs_to(:user, User, type: FlakeId.Ecto.Type)
    belongs_to(:project, Project, type: FlakeId.Ecto.Type)
    belongs_to(:tier, Tier, type: FlakeId.Ecto.Type)
    field(:money, Money.Ecto.Map.Type)
    field(:day_of_month, :integer)
    field(:last_paid_at, :naive_datetime)

    timestamps()
  end

  @doc false
  def changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [])
    |> validate_required([])
  end
end
