defmodule Basedpay.Repo.Migrations.CreateInitialSchema do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add(:id, :uuid, primary_key: true)

      timestamps()
    end

    create table(:gateway_users, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:user_id, references(:users, type: :uuid, on_delete: :delete_all), null: false)
      add(:gateway, :string, null: false)
      add(:gateway_id, :string, null: false)

      timestamps()
    end

    create unique_index(:gateway_users, [:user_id, :gateway])

    create table(:projects, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:user_id, references(:users, type: :uuid, on_delete: :delete_all), null: false)
      add(:name, :string, default: "", null: false)
      add(:content, :text, default: "", null: false)

      timestamps()
    end

    create table(:tiers, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:project_id, references(:projects, type: :uuid, on_delete: :delete_all), null: false)
      add(:name, :string, default: "", null: false)
      add(:content, :text, default: "", null: false)
      add(:money, :map, null: false)

      timestamps()
    end

    create table(:subscriptions, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:user_id, references(:users, type: :uuid, on_delete: :delete_all), null: false)
      add(:project_id, references(:projects, type: :uuid, on_delete: :delete_all), null: false)
      add(:tier_id, references(:tiers, type: :uuid, on_delete: :nothing))
      add(:money, :map, null: false)
      add(:day_of_month, :integer, null: false)
      add(:last_paid_at, :naive_datetime)

      timestamps()
    end

    create table(:payment_methods, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:user_id, references(:users, type: :uuid, on_delete: :delete_all), null: false)
      add(:gateway, :string, null: false)
      add(:gateway_id, :string, null: false)
      add(:gateway_data, :map, null: false, default: %{})

      timestamps()
    end

    create table(:payments, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:user_id, references(:users, type: :uuid, on_delete: :delete_all), null: false)
      add(:payment_method_id, references(:payment_methods, type: :uuid, on_delete: :nothing))
      add(:subscription_id, references(:subscriptions, type: :uuid, on_delete: :nothing))
      add(:project_id, references(:projects, type: :uuid, on_delete: :nothing))
      add(:money, :map, null: false)
      add(:gateway, :string, null: false)
      add(:gateway_id, :string, null: false)
      add(:gateway_data, :map, null: false, default: %{})

      timestamps()
    end
  end
end
